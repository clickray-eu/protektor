$(document).ready(function () {

	slickMenu();

	// additional wrapper on first depth li in menu 
	$('.main-menu" ul > li.hs-menu-depth-1 > a').wrap('<span></span>');
	$('.main-menu" ul > li.hs-menu-depth-1 > ul > li').each(function () {

		if ($(this).children("ul").length == 0) {
			$(this).parent().addClass('no-children-of-children')
			return false;
		}
	})

})

function slickMenu(){

	 $('.header .main-menu .hs-menu-wrapper > ul').slicknav({
		label: '',
		appendTo:'.header > div ',
		// init: function a() {
        //     var c = $(".header .small-menu ul > li").clone(true);
		// 	$("header.header .slicknav_nav").append(c)        
		// }
	});


}